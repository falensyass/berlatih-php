<?php
function tentukan_nilai($number)
{
    if ($number >= 85) {
    	$nilai = "Sangat Baik";
    }elseif ($number < 85  && $number >= 70) {
    	$nilai = "Baik";
    }elseif ($number < 70 && $number >= 60) {
    	$nilai = "Cukup";
    }else {
    	$nilai = "Kurang";
    }
    return $nilai .  "<br>";
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>